# -*- mode: python -*-

block_cipher = None


a = Analysis(['__main__.py'],
             pathex=['.'],
             binaries=[('/System/Library/Frameworks/Tk.framework/Tk', 'tk'), ('/System/Library/Frameworks/Tcl.framework/Tcl', 'tcl')],
	         datas=[('../mufinder-env/lib/python3.7/site-packages/ase/spacegroup/spacegroup.dat','ase/spacegroup/'),('../mufinder-env/lib/python3.7/site-packages/soprano/data','soprano/data'),('mufinder_logo.ico','.')],             
			 hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='MuFinder',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          runtime_tmpdir=None,
          console=False )
app = BUNDLE(exe,
             name='MuFinder.app',
             icon='mufinder_logo.icns',
             info_plist={
        'NSHighResolutionCapable': 'True'
        },
             bundle_identifier=None)

import shutil, os
shutil.copyfile('mufinder.ini', '{0}/mufinder.ini'.format(DISTPATH))
shutil.copyfile('mufinder.ini',os.path.join(os.getcwd(),'dist','MuFinder.app','Contents','MacOS','mufinder.ini'))
shutil.copytree('script_templates',os.path.join(os.getcwd(),'dist','MuFinder.app','Contents','MacOS','script_templates'))
shutil.copytree('script_templates',os.path.join(os.getcwd(),'dist','script_templates'))

