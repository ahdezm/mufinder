from muesr.core import Sample
from muesr.i_o import load_cif
import os.path
import pandas
import numpy as np

def write_mag_array(cif_file, outputtext,num_q):
    xtal = Sample()
    load_cif(xtal,cif_file)
    syms=xtal.cell.symbols
    pos=xtal.cell.get_scaled_positions()
    mag_file=cif_file.replace('.cif', '.mag_array')
    if os.path.exists(mag_file)==True:
        outputtext.insert("end-1c",'Mag_array file already exists.\n')
    else:
        # write pandas dataframe
        a=[pos[i][0] for i in range(len(xtal.cell.symbols))]
        b=[pos[i][1] for i in range(len(xtal.cell.symbols))]
        c=[pos[i][2] for i in range(len(xtal.cell.symbols))]
        zeros=[0]*len(xtal.cell.symbols)
        mag_array_data = {'elem':xtal.cell.symbols,'a': a, 'b':b, 'c':c,'mxr':zeros,
        'mxi':zeros,'myr':zeros,'myi':zeros,'mzr':zeros,'mzi':zeros}
        mag_array_df = pandas.DataFrame(mag_array_data, columns= ['elem', 'a','b','c',
        'mxr','mxi','myr','myi','mzr','mzi'])
        
        with open(mag_file, 'w') as (f):
            for i in range(num_q):
                #f.write('! coefficents for propagation vector k%i\n' %(i+1))
                mag_array_df.to_csv(f,index = None,header=True)
        outputtext.insert("end-1c",'Generated mag_array file %s.\n' %mag_file)
      
def load_mag_array(mag_file,num_q):
    # read in mag_array file
    df=pandas.read_csv(mag_file,comment='!')
    df=df[df["elem"] != 'elem']
    df[["mxr", "mxi","myr","myi","mzr","mzi"]] = df[["mxr", "mxi","myr","myi","mzr",
    "mzi"]].apply(pandas.to_numeric)
  
    no_atoms=int(len(df)/num_q) #infer number of atoms
    full_mag_array=[]
    for i in range(num_q):
        mag_array=[]
        for j in range(no_atoms):
            k=no_atoms*i+j
            mag_array.append([df.iloc[k]['mxr']+df.iloc[k]['mxi']*1j, df.iloc[k]['myr']+
            df.iloc[k]['myi']*1j, df.iloc[k]['mzr']+df.iloc[k]['mzi']*1j])
        full_mag_array.append(mag_array)       
    
    return full_mag_array
