from muesr.core import Sample
from muesr.engines.clfc import find_largest_sphere, locfield
from muesr.i_o import load_cif
import numpy as np
import os

from tkinter import simpledialog
from widgets.textfield import TextField

def ic_calc(xtal, n, radius, outputtext, cif_file, field_dist,lorentz,shape_factor):
    r=locfield(xtal,'i', [n,n,n], radius,nnn=3,nangles=100000)
    n_bins=1000
    hist=np.zeros(n_bins)
    hist_tot=np.zeros(n_bins)
    bin_range=np.zeros(n_bins+1)

    no_muons = len(r)
    
    max_field = np.linalg.norm(r[0].D+r[0].L, axis=1)
    min_field = np.linalg.norm(r[0].D+r[0].L, axis=1)
    for i in range(0, no_muons):
        if np.any(np.linalg.norm(r[i].D+r[i].L, axis=1) > max_field):
            max_field = np.linalg.norm(r[i].D+r[i].L, axis=1)
        if np.any(np.linalg.norm(r[i].D+r[i].L, axis=1) < min_field):
            min_field = np.linalg.norm(r[i].D+r[i].L, axis=1)
        max_field = np.amax(max_field)
        max_hist = max_field*1.01
        min_field = np.amin(min_field)
        min_hist = min_field*0.99
        if min_hist > 0:
            min_hist = 0

    for i in range(0, no_muons):
        if lorentz==True:
            hist, bin_range = np.histogram(np.linalg.norm(r[i].D+(1-3*shape_factor)*r[i].L, axis=1), bins=n_bins, range=(min_hist,max_hist))
        else:
            hist, bin_range = np.histogram(np.linalg.norm(r[i].D, axis=1), bins=n_bins, range=(min_hist,max_hist))
        hist_tot += hist

    mid_of_bin = bin_range[0:-1]+0.5*np.diff(bin_range)

    filename=cif_file.replace('.cif',"_hist.txt")
    np.savetxt(filename, np.c_[mid_of_bin, hist_tot], delimiter=',', fmt='%e')
   
    outputtext.insert('end-1c',
    'Field distribution saved as %s. Click "View plot" to generate plot.\n' %filename)
    field_dist.replace(filename)
   
