import numpy as np
import math
from muesr.core import Sample
from muesr.engines.clfc import find_largest_sphere, locfield, dipten
from muesr.i_o import load_cif
from muesr.utilities import muon_find_equiv
from ase import io
from ase import Atoms
from ase.build.supercells import make_supercell
import os

from tkinter import simpledialog

from dipolar.mag_array import write_mag_array, load_mag_array
from dipolar.distorted import distortion_correction
from dipolar.ic import ic_calc
   
# function to generate magnetic unit cell  
def get_MUC(incomm, structure, num_q, k, mag_array_q): 
    s=[]
    
    if incomm==False:
        for i in k[0]:
            if float(i) !=0:
                sx=int(1./float(i))
            else:
                sx=1
            s.append(sx)
    else:
        s=[3,3,3] # MUC doesn't exist for IC magnetic structures
    MUC=make_supercell(structure, np.diag(s))
    structure.set_atomic_numbers([x+1 for x in range(len(structure.get_atomic_numbers()))])
    MUC_copy=make_supercell(structure, np.diag(s))
  
    full_mag_array=[]
    for q in range(num_q):
        single_mag_array=[]
        k_vec=np.array([float(k[q][0]), float(k[q][1]), float(k[q][2])])
        mag_array=mag_array_q[q]  
        for i, atom_no in enumerate(MUC_copy.get_atomic_numbers()):
                        R_vec=(MUC.get_scaled_positions()[i]-MUC.get_scaled_positions()[atom_no-1])
                        phase=2*math.pi*np.dot(k_vec,R_vec @ np.diag(s))
                        single_mag_array.append([np.exp(-phase*1.j)*x for x in mag_array[atom_no-1]])  
        if q==0:
            full_mag_array=single_mag_array
            
        else:
            full_mag_array=np.add(full_mag_array,single_mag_array)
    
    # rescale moments to a reasonable size for visualisation
    if abs(np.max(np.real(full_mag_array)))!=0:
        scale=5/abs(np.max(np.real(full_mag_array)))
    else:
        scale=5
    MUC.set_momenta(scale*np.real(full_mag_array))
    return MUC
     
               
def enter_pos(outputtext):        
    outputtext.insert("end-1c",'Enter user-defined muon positions.\n')
    muon_pos=[]
    indices=[]
    num=1
    while True:    
        a = simpledialog.askstring("Input",'Enter fractional coordinates for muon position %i.' 
        ' Type "done" if finished.\n' %num)
        if a=='done':
            break
        else:
            try:
                muon_pos.append([float(a.split()[0]),float(a.split()[1]),float(a.split()[2])])
                indices.append(num)
                num+=1
            except ValueError:
                outputtext.insert("end-1c",'Coordinates entered incorrectly. Please try again.\n')
    return muon_pos,indices
	
def calc_field(mu_struct,options,indices,mupos,cif_file,num_q,k,dipole_fields,n,outputtext,mag_file, field_dist,shape_factor):
         
        # Destructuring options
        incommensurate, distorted, mode, find_equiv, vector, lorentz=options

        # Define crystal structure
        xtal = Sample()
        load_cif(xtal,cif_file)

        # Define magnetic structure
        xtal.new_mm()
        xtal.mm.k = np.array([k[0][0], k[0][1], k[0][2]])

        mag_array=load_mag_array(mag_file,num_q)[0] # just get mag_array for first k
        xtal.mm.fc=np.array(mag_array) 

        # Add muons
        if mode=="manual":
            for pos in mupos:
                xtal.add_muon(pos)
                
        if mode=="auto":
            for index in indices:
                muon_pos=mu_struct.muon_sites[index]
                xtal.add_muon([muon_pos[0], muon_pos[1], muon_pos[2]])
 
        if find_equiv==True:
            old_len=len(xtal.muons)
            muon_find_equiv(xtal)
            new_len=len(xtal.muons)
            new_index=[]
            for i in range(old_len):
               for j in range(int(new_len/old_len)): 
                   new_index.append(indices[i])
            indices=new_index
        
        radius = find_largest_sphere(xtal,[n,n,n])
        if incommensurate==True:
            ic_calc(xtal, n, radius, outputtext, cif_file, field_dist,lorentz,shape_factor)
            
        else:
            r = locfield(xtal, 's', [n,n,n], radius)
        
            no_muons = len(r)

            if distorted=='yes':
                # include corrections to dipolar field due to distortions of nearby magnetic
                # atoms
                r_corr=distortion_correction(mu_struct,muon_pos,indices, cif_file,k,mag_array,'field')
        
            else:
                r_corr=[]
                for i in range(0, no_muons):
                    r_corr.append([0, 0, 0])
          
            r_tot=[]
            for i in range(0, no_muons):
                if lorentz==True:
                    r_tot.append(r[i].D+r_corr[i]+(1-3*shape_factor)*r[i].L)
                else:
                    r_tot.append(r[i].D+r_corr[i])
            
            if vector==True:
                B_avg=[0,0,0]
                for i in range(0, no_muons):
                    outputtext.insert('end-1c','Muon site %i has Bdip = %s T\n' %(indices[i],r_tot[i]))
                    B_avg+=r_tot[i]
                if no_muons>1:
                    B_avg=B_avg/no_muons
                    outputtext.insert('end-1c','Average dipolar field is %s T\n'%B_avg)   
                    
            else:
                B_avg=0
                # Print fields to screen
                for i in range(0, no_muons):
                    outputtext.insert('end-1c','Muon site {:2d} has Bdip = {:.6f} T\n'.format(indices[i],np.linalg.norm(r_tot[i],axis=0)))
                    B_avg+=np.linalg.norm(r_tot[i],axis=0)
                if no_muons>1:
                    B_avg=B_avg/no_muons
                    outputtext.insert('end-1c','Average dipolar field is {:.6f} T\n'.format(B_avg))
            
            # store dipolar fields
            dipole_fields[0]=r

# similar to above function, but for dipolar tensor
# commensurate structures only!
def calc_tensor(mu_struct,options,indices,mupos,cif_file,num_q,k,n,outputtext,mag_file):
         
        # Destructuring options
        incommensurate, distorted, mode, find_equiv, vector, lorentz=options

        # Define crystal structure
        xtal = Sample()
        load_cif(xtal,cif_file)

        # Define magnetic structure
        xtal.new_mm()
        xtal.mm.k = np.array([k[0][0], k[0][1], k[0][2]])

        mag_array=load_mag_array(mag_file,num_q)[0] # just get mag_array for first k
        xtal.mm.fc=np.array(mag_array) 

        # Add muons
        if mode=="manual":
            for pos in mupos:
                xtal.add_muon(pos)
                
        if mode=="auto":
            for index in indices:
                muon_pos=mu_struct.muon_sites[index]
                xtal.add_muon([muon_pos[0], muon_pos[1], muon_pos[2]])
 
        if find_equiv==True:
            old_len=len(xtal.muons)
            muon_find_equiv(xtal)
            new_len=len(xtal.muons)
            new_index=[]
            for i in range(old_len):
               for j in range(int(new_len/old_len)): 
                   new_index.append(indices[i])
            indices=new_index
        
        radius = find_largest_sphere(xtal,[n,n,n])
        
        
        D=dipten(xtal, [n,n,n], radius)

        no_muons = len(D)

        if distorted=='yes':
            # include corrections to dipolar field due to distortions of nearby magnetic
            # atoms
            D_corr=distortion_correction(mu_struct,muon_pos,indices, cif_file,k,mag_array,'tensor')
        
        else:
            D_corr=[]
            for i in range(0, no_muons):
                D_corr.append([[0, 0, 0],[0,0,0],[0,0,0]])
          
        D_tot=[]
        for i in range(0, no_muons):
            D_tot.append(D[i]+D_corr[i])
        
        D_avg=[[0, 0, 0],[0,0,0],[0,0,0]]   
        
        # Print fields to screen
        for i in range(0, no_muons):
            outputtext.insert('end-1c','Muon site %i has Ddip = %s T/\N{GREEK SMALL LETTER MU}_B\n' %(indices[i],D_tot[i]))
            D_avg+=D_tot[i]
        if no_muons>1:
            D_avg=D_avg/no_muons
            outputtext.insert('end-1c','Average dipolar tensor is %s T/\N{GREEK SMALL LETTER MU}_B\n' %(D_avg)) 