# -*- mode: python -*-

block_cipher = None


a = Analysis(['__main__.py'],
             pathex=['.'],
             binaries=[],
             datas=[('../mufinder-env/lib/python3.7/site-packages/ase/spacegroup/spacegroup.dat','ase/spacegroup/'),('../mufinder-env/lib/python3.7/site-packages/soprano/data','soprano/data'),('mufinder_logo.png','.')],
             hiddenimports=['pkg_resources.py2_warn'],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='MuFinder',
          icon='mufinder_logo.png',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          runtime_tmpdir=None,
          console=True )

import shutil, os
shutil.copyfile('mufinder.ini', '{0}/mufinder.ini'.format(DISTPATH))
shutil.copyfile('mufinder_logo.png', '{0}/mufinder_logo.png'.format(DISTPATH))
shutil.copytree('script_templates',os.path.join(os.getcwd(),'dist','script_templates'))
