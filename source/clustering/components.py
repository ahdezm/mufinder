#!/usr/bin/python

import logging

import numpy as np
import networkx as nx

import spglib as spg

from ase.geometry import get_distances
from ase.spacegroup import Spacegroup


def connected_components(input_struct, muon_sites, param, logger=None):
    logger = logging.getLogger(logger)

    tol = float(param)
    muon_pos = input_struct.cell.cartesian_positions(muon_sites)

    sg_N = spg.get_symmetry_dataset(input_struct)["number"]
    sg = Spacegroup(sg_N, 1)

    # construct distance matrix
    logger.info("Constructing distance matrix...")

    rows = []

    # TODO: Consider using single get_distances call instead of iterating

    for site in muon_sites:
        equiv_frac, _ = sg.equivalent_sites(
            site,
            onduplicates="warn",
            symprec=0.001
        )

        equiv_pos = input_struct.cell.cartesian_positions(equiv_frac)

        _, distances = get_distances(
            muon_pos, equiv_pos,
            cell=input_struct.cell,
            pbc=input_struct.pbc
        )

        rows.append(np.min(distances, axis=1))

    dist_mat = np.vstack(rows)

    # construct adjacency matrix
    adj = dist_mat < tol
    adj[np.diag_indices_from(adj)] = 0
    graph = nx.from_numpy_matrix(adj.astype(int))

    # find connected subgraphs
    logger.info("Finding connected subgraphs...")

    subgraphs = []

    for c in nx.connected_components(graph):
        subgraphs.append(list(graph.subgraph(c).nodes))

    return subgraphs
