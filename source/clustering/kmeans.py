#!/usr/bin/python

from operator import itemgetter
from soprano.analyse.phylogen import PhylogenCluster, Gene


def kmeans_clustering(resultsColl, muon_label, params):
    num_clusters = int(params)

    # Cluster according to energy and bond order parameters around the muon
    geneE = Gene('energy')
    geneBOrd = Gene('bond_order_pars', params={'s1': muon_label})

    pylogen = PhylogenCluster(resultsColl, genes=[geneBOrd, geneE])

    inds, composition = pylogen.get_kmeans_clusters(num_clusters)
    clusters = [cluster.tolist() for cluster in composition]

    return list(sorted(clusters, key=itemgetter(0)))
