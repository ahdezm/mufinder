import spglib as spg
import numpy as np
from ase import Atoms
from numpy import linalg as LA

from ase.build.supercells import make_supercell
from ase.symbols import Symbols, symbols2numbers

# hide Matplotlib warning that comes with pyinstaller
import warnings
warnings.filterwarnings("ignore", "(?s).*MATPLOTLIBDATA.*", category=UserWarning)

import matplotlib
matplotlib.use("TkAgg")
from matplotlib import pyplot as plt

from ase.data.colors import jmol_colors
from matplotlib.lines import Line2D
from matplotlib.patches import Patch
       
def supercell_target(single_cell, structure):
    supercell_m=structure.get_cell() @ LA.inv(single_cell.get_cell())
    supercell_mat=(np.round(supercell_m)).astype(int)     
    scell_species=make_supercell(single_cell, supercell_mat) 
    scell=make_supercell(single_cell, supercell_mat)
    return scell
  
def radial_disp(cif_file,structure,musym):
    scell_input=supercell_target(cif_file,structure)
    combined=scell_input+structure
    
    # get displacements of all of the atoms in the cell
    dist_mat=combined.get_all_distances(mic=True)
        
    # create mask to check that we are comparing the same element
    for i, num_i in enumerate(combined.get_atomic_numbers()):
        for j, num_j in enumerate(combined.get_atomic_numbers()):
            if num_i!=num_j:
                dist_mat[i][j]=np.nan
            if i==j:
                dist_mat[i][j]=np.nan 
        
    displacement_mat=combined.get_all_distances(mic=True, vector=True)
    muon_disp=[]

    for i in range(len(scell_input.get_atomic_numbers())):
        muon_disp.append(displacement_mat[np.where(combined.get_atomic_numbers()==symbols2numbers(musym))[0][0]][i])
    combined.get_atomic_numbers()
        
    displacements=[]
    for i in range(len(scell_input.get_atomic_numbers())):
        displacements.append(displacement_mat[i][np.nanargmin(dist_mat[i])])

    radial_dist=[]
    muon_dist=[]
    for i in range(len(scell_input.get_atomic_numbers())):
        radial_dist.append(projected_distance(displacements[i],muon_disp[i]))
        muon_dist.append(np.linalg.norm(muon_disp[i]))

    legend=[]
    for i in range(len(scell_input.get_atomic_numbers())):
        legend.append(jmol_colors[scell_input.get_atomic_numbers()[i]])
        
        
    fig=plt.figure()
    plt.scatter(muon_dist, radial_dist, c=np.array(legend),edgecolor='black')
    plt.ylabel('Radial displacement ($\mathrm{\AA}$)')
    plt.xlabel('Distance from muon site ($\mathrm{\AA}$)')
        
    elem_list=scell_input.get_chemical_symbols()
    elem_list=list(set(elem_list))
    color_list=[]
    for i in elem_list: 
        color_list.append(jmol_colors[symbols2numbers(i)][0])
        
    color_list=np.array(color_list)
        
    legend_elements=[]
    for i in range(len(color_list)):
        legend_elements.append(Line2D([0], [0], marker='o', color='w', label=elem_list[i],
                          markerfacecolor=color_list[i],markeredgecolor='black'))
                                    
    plt.legend(handles=legend_elements)

    plt.show()
        
def projected_distance(vec1,vec2):
    # returns projection of vec1 along direction of vec 2
    return np.dot(vec1,vec2)/np.linalg.norm(vec2)
    
def max_displacement(cif_file,structure):
    distances=[]
    scell_input=supercell_target(cif_file,structure)

    # combine both structures
    combined=structure+scell_input

    dist_mat=combined.get_all_distances(mic=True)
        
    # create mask to check that we are comparing the same element

    for i, num_i in enumerate(combined.get_atomic_numbers()):
        for j, num_j in enumerate(combined.get_atomic_numbers()):
            if num_i!=num_j:
                dist_mat[i][j]=np.nan
            if i==j:
                dist_mat[i][j]=np.nan 

    distances=[]
    for i in range(len(combined.get_atomic_numbers())):
        distances.append(np.nanmin(dist_mat[i]))
    
    return np.nanmax(distances)
  
