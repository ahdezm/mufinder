#!/usr/bin/python
import spglib as spg
import numpy as np
import numpy.linalg as LA
from ase import Atoms
from ase.geometry import cell_to_cellpar
from ase.spacegroup import crystal

import networkx as nx

# function to generate symmetry equivalent positions
def gen_sym_equiv(basis, structure, label):
    spacegroup=spg.get_symmetry_dataset(structure)
    replicate = crystal(label, basis,spacegroup=spacegroup['number'], 
    cellpar=cell_to_cellpar(structure.get_cell())) 
    return replicate

# function to generate single muon in cell
def gen_single_muon(basis, structure, label):
    replicate = crystal(label, basis, spacegroup=1,   cellpar=cell_to_cellpar(structure.get_cell())) 
    return replicate

def add_sites(mu_struct):
    input_struct=mu_struct.input_struct
    new_label=mu_struct.new_label
    muon_sites=mu_struct.muon_sites
    count=0    
    for site_i in muon_sites:
        muon=crystal([new_label], site_i, spacegroup=1, cellpar=cell_to_cellpar(input_struct.get_cell()))
        if count==0:
            all_muons=muon
        else: 
            all_muons=all_muons+muon
        count+=1
    
    all_muons+=input_struct # embed muons in structure
    return all_muons
                   
def shift_sites(mu_struct):
    input_struct=mu_struct.input_struct
    new_label=mu_struct.new_label
    muon_sites=mu_struct.muon_sites
    count=0    
    for cluster in mu_struct.clust_composition: 
    # find member of each subgraph that is closest to the origin, but still inside cell
        dist_origin=[]
        sub_muon_sites=[muon_sites[p] for p in cluster]
        for site in sub_muon_sites:
            replica_i=gen_sym_equiv(site, input_struct, ['O'])
            distances=[]
            for single_mu in replica_i: 
                muon_cart=single_mu.position
                if all(coord>=0 for coord in muon_cart):
                    distances.append(LA.norm(muon_cart))
            dist_origin.append(min(distances))
        origin_index=np.argmin(dist_origin)

	    # translate each member of subgraph to be closest to the member that 
	    # is closest to the origin
    
        replica_1=gen_sym_equiv(muon_sites[origin_index], input_struct, ['H']) 
        distances=[]
        muon_cart_pos=[]
        for single_mu in replica_1:   
                muon_cart=single_mu.position 
                if all(coord>=0 for coord in muon_cart):
                    muon_cart_pos.append(muon_cart)
                    distances.append(LA.norm(muon_cart))
        muon_frac_pos=LA.inv(replica_1.cell) @ muon_cart_pos[np.argmin(distances)]
        replica_1=gen_sym_equiv(muon_frac_pos, input_struct, ['H'])
        sub_muon_sites=[muon_sites[p] for p in cluster]
        for site_i in sub_muon_sites:
            replica_i=gen_sym_equiv(site_i, input_struct, ['O'])
            combined=replica_i+replica_1
            atoms1 = [x for x in range(len(combined.get_atomic_numbers())) 
            if combined.get_chemical_symbols()[x]=='O']
            atoms2 = [y for y in range(len(combined.get_atomic_numbers())) 
            if combined.get_chemical_symbols()[y]=='H']
            distances=combined.get_distances(atoms1, atoms2[0], mic=False)
            index=np.argmin(distances)
            closest=crystal([new_label], replica_i.get_scaled_positions()[index], spacegroup=1, cellpar=cell_to_cellpar(replica_i.get_cell()))
            if count==0:
                shifted=closest
            else: 
                shifted=shifted+closest
            count+=1
    
    # get order that sites are added to 'shifted'
    site_order=[]
    for cluster in mu_struct.clust_composition:
        for p in cluster:
            site_order.append(p)
    
    unsrt_pos=shifted.get_positions()
    sort_pos=[x for _,x in sorted(zip(site_order,unsrt_pos))]
    shifted.set_positions(sort_pos) 
    shifted+=input_struct # embed muons in structure
    
    return shifted
   
