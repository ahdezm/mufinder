#!/usr/bin/python

import os
import re
import math
import mmap
import time
import logging

import tkinter as tk

from pathlib import Path

import numpy as np
from numpy import linalg as LA

from ase import io
from ase.data import atomic_numbers
from ase.calculators.singlepoint import SinglePointCalculator
from soprano.collection import AtomsCollection

from clustering.displacements import max_displacement
from clustering.components import connected_components
from clustering.kmeans import kmeans_clustering
from clustering.shift import add_sites, shift_sites


class MuonLabelError(Exception):
    pass


class CastepEnergyError(Exception):
    pass


def muon_label(path):
    """ Automatically detect the label used for the muon in a file. """
    with open(path) as f:
        contents = f.read()

        if 'H:mu' in contents:
            return 'H:mu'

        if 'H ' in contents:
            return 'H'

    return None


def muon_relabel(output_files, old_label, new_label):
    """ Write new set of output files replacing old_label with new_label. """
    renamed_files = []

    for path in output_files:
        renamed = os.path.join(
            os.path.dirname(path),
            'tmp_' + os.path.basename(path)
        )

        with open(path, 'r') as f1:
            with open(renamed, 'w') as f2:
                for line in f1:
                    f2.write(line.replace(old_label, new_label))

        renamed_files.append(renamed)

    return renamed_files


def castep_final_energy(path):
    energy = None
    needles = [
        b"NB est. 0K energy",
        b"NB dispersion corrected est. 0K energy",
        b"Final energy, E",
    ]

    with open(path) as f:
        with mmap.mmap(f.fileno(), 0, access=mmap.ACCESS_READ) as s:
            for needle in needles:
                start = s.rfind(needle)

                if start == -1:
                    continue

                end = s.find(b"\n", start)
                energy = s[start:end].decode("utf-8")
                break

    if energy is not None:
        m = re.search(r"(-?\d+\.?\d+)\s+eV", energy)
        if m:
            return float(m.group(1))
        else:
            raise CastepEnergyError(energy)
    else:
        raise CastepEnergyError(energy)


def read_results(cell_files, castep_files, label="", alt_label="Pu",logger=None):
    logger = logging.getLogger(logger)

    # Get label for muon if not specified by user
    if label == "":
        label = muon_label(cell_files[0])
        if not label:
            logger.error(
                'Unable to detect muon label in %s. Aborting...',
                cell_files[0]
            )
            raise MuonLabelError

    # ASE cannot properly interpret H:mu, here we relabel the muon to fix this
    if label == 'H:mu':
        # Hopefully your sample doesn't contained plutonium!
        cell_files_tmp = muon_relabel(cell_files, label, alt_label)

        results = read_results(
            cell_files_tmp, castep_files,
            label=alt_label, logger=logger.name
        )

        # delete temporary files
        for file in cell_files_tmp:
            os.remove(file)

        return results

    logger.info("Loading structures...")

    cout_atoms = []
    cout_energies = []

    for cell_path, energy_path in zip(cell_files, castep_files):
        cell = io.read(cell_path, calculator_args={"keyword_tolerance": 3})

        try:
            energy = castep_final_energy(energy_path)
        except (CastepEnergyError, FileNotFoundError):
            logger.warn("Unable to read energy from %s", energy_path)
            continue

        calc = SinglePointCalculator(cell, energy=energy)
        cell.set_calculator(calc)

        cout_atoms.append(cell)
        cout_energies.append(energy)

    resultsColl = AtomsCollection(cout_atoms)

    logger.info("Loaded %s structures.", len(resultsColl))

    # Extract the list of energies and append it to the collection
    E = np.array(cout_energies)
    resultsColl.set_array('energy', E)
    resultsColl = resultsColl.sorted_byarray('energy')  # Sort by energy

    return resultsColl, label


def cluster(algorithm, shift, muon_label, params, out_path,
            output_text, aout, use_max_disp, max_disp):

    start_time = time.time()

    logger = logging.getLogger("cluster")

    # Loading CASTEP output files
    cell_files = list(Path(out_path).glob("*-out.cell"))
    castep_files = [
        path.with_name(re.sub(r"-out\.cell$", ".castep", path.name))
        for path in cell_files
    ]

    # Load collection of results
    try:
        resultsColl, new_label = read_results(
            cell_files, castep_files,
            label=muon_label, logger=logger.name + ".read_results"
        )
    except ValueError:
        return

    # Extract fractional coordinates of muon sites and translate these to
    # coordinates in the conventional cell if a supercell has been used
    muon_sites = []
    muon_sites_supercell = []
    valid_struct = []

    input_inv = LA.inv(aout.input_struct.get_cell())
    muon_number = atomic_numbers[new_label]

    for structure in resultsColl.structures:
        if use_max_disp:
            max_disp_struct = max_displacement(aout.input_struct, structure)
        else:
            max_disp_struct = 0.0
            max_disp = 1.0

        scell_scale = structure.get_cell() @ input_inv

        ind = np.flatnonzero(structure.numbers == muon_number)
        muon_pos = ind and structure.get_scaled_positions()[ind]

        # check for 'broken' output files and maximum allowed displacement
        if max_disp_struct <= float(max_disp):
            muon_sites_supercell.append(muon_pos[0])
            muon_sites.append([math.modf(x)[0]
                               for x in (muon_pos[0] @ scell_scale)])
            valid_struct.append(structure)

    # TODO: Add error message when no muons are found
    aout.new_label = new_label
    aout.valid_struct = valid_struct
    aout.muon_sites = muon_sites
    aout.muon_sites_supercell = muon_sites_supercell

    if algorithm == 'connected components':
        aout.clust_composition = connected_components(
            aout.input_struct, aout.muon_sites, params,
            logger="cluster.connected_components"
        )
    elif algorithm == 'k-means':
        aout.clust_composition = kmeans_clustering(resultsColl, new_label, params)

    logger.info("Clusters are: %s", aout.clust_composition)

    if shift:
        logger.info(
            ("Shifting muon positions to occupy smaller subspace of the unit"
             "cell...")
        )
        aout.shifted = shift_sites(aout)
    else:
        aout.shifted = add_sites(aout)

    logger.info("Done!")

    time_taken = time.time() - start_time
    logger.info("Finished in %s seconds.", round(time_taken, 2))
