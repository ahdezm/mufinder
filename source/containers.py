#!/usr/bin/python
import numpy as np

"""Class defining object to hold parameters for site generation"""
class mu_gen:
    
    def __init__(self):
        self.input_struct=[]
        self.symbol='H:mu'
        self.site_list=[]
        self.minr=0.5
        self.vdws=1.0
        self.scell_mat=np.diag([1,1,1])
        self.mode='fractional'

"""Class defining object to hold results of clustering analysis"""
class mu_struct:
    
    def __init__(self):
        self.input_struct=[]
        self.new_label=''
        self.valid_struct=[None]
        self.muon_sites=[None]
        self.muon_sites_supercell=[]
        self.shifted=[None]
        self.clust_composition=[]