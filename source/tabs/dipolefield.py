from tkinter import *
from tkinter import ttk
import tkinter.scrolledtext as tkst

from widgets.tooltip import ToolTip, ToolTipBase
from widgets.table import SimpleTableInput
from widgets.get_numbers import get_numbers
from widgets.textfield import TextField

from dipolar.mag_array import *
from dipolar.calcfield import *

from custom_view import *
from config import *

import threading

# hide Matplotlib warning that comes with pyinstaller
import warnings
warnings.filterwarnings("ignore", "(?s).*MATPLOTLIBDATA.*", category=UserWarning)

import matplotlib
matplotlib.use("TkAgg")
from matplotlib import pyplot as plt

class dipolefield(ttk.Notebook):  
    
    def mode_update(self):
        if self.mode.get()!=self.current_mode:
            if self.mode.get()=='manual':
                self.d['menu'].invoke(1)
                self.inputnums.make_inactive()
            else:
                self.inputnums.make_active()
            self.current_mode=self.mode.get()

        if self.distorted.get()!=self.current_dist:
            if self.distorted.get()=='yes':
                self.m['menu'].invoke(0)
                self.fe.config(state=DISABLED)
                self.find_equiv.set(0)
                self.ic.config(state=DISABLED)
                self.incomm.set(0)
            else:
                self.fe.config(state=NORMAL)
                self.ic.config(state=NORMAL)
            self.current_dist=self.distorted.get()
        self.after(1000, func=self.mode_update)

    def set_struct(self):
        try:
            input_struct=filedialog.askopenfilename()
            self.input.replace(os.path.normpath(input_struct))
            aout.input_struct=io.read(input_struct)
            self.outputtext.insert("end-1c","%s loaded as input cif file.\n" %input_struct)
            try:
                custom_view(aout.input_struct) 
            except UnicodeDecodeError:
                    pass      
        except: 
            self.outputtext.insert("end-1c","Error loading file.  Please try again.\n")

    def calculate_fields(self):
        if self.mode.get()=='auto':
            indices=get_numbers(self.inputnums.get(),self.outputtext)
            mupos=None
        else:
            mupos, indices=enter_pos(self.outputtext)
        if self.property.get()=='Vector':
            vector=True
        else:
            vector=False
        options=[self.incomm.get(), self.distorted.get(),self.mode.get(),
        self.find_equiv.get(),vector,self.lorentz.get()]
        if self.property.get()=='Magnitude' or self.property.get()=='Vector':
            threading.Thread(target=calc_field, args=(aout, options, indices, mupos,self.input.get(),
            1,self.k.get(), dipole_fields, int(self.n.get()),
            self.outputtext,self.mag_array_name.get(),self.field_dist,float(self.shape_factor.get()))).start()
        if self.property.get()=='Dipolar tensor':
            threading.Thread(target=calc_tensor, args=(aout, options, indices, mupos,self.input.get(),
            1,self.k.get(), int(self.n.get()),
            self.outputtext,self.mag_array_name.get())).start()

    def gen_magarray(self):
        write_mag_array(self.input.get(),self.outputtext,1)
        self.mag_array_name.replace(self.input.get().replace('.cif', '.mag_array'))
    
    def view_mag_struct(self):
        struct=io.read(self.input.get())
        mag_array=load_mag_array(self.mag_array_name.get(),1)
        custom_view(get_MUC(self.incomm.get(),struct,1,self.k.get(),mag_array))
        
    def view_plot(self):
        data=np.loadtxt(open(self.field_dist.get(), "rb"), delimiter=",")
        plt.plot(data[:,0],data[:,1])
        plt.ylabel('P(B)')
        plt.xlabel('B[T]')
        plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
        plt.show()
        
    def __init__(self, *args, **kwargs):

        self.current_mode='auto'
        self.current_dist='no'
        
        ttk.Frame.__init__(self, *args, **kwargs)
        self.input=TextField(self, "Input structure:", 0, 0, '', None)
        ttk.Button(self, text="load", width=10, command=self.set_struct).grid(row=2, 
        column=0, sticky=W)

        self.distorted=StringVar(self)
        distorted_options=['yes','no']
        Label(self, text="Use distorted structures?").grid(row=3, column=0, sticky=W)
        self.d = ttk.OptionMenu(self, self.distorted, distorted_options[1], 
        *distorted_options)
        self.d.grid(row=4, column=0, sticky=W)

        self.inputnums=TextField(self, "Muon sites to include", 7, 0, '',
        'Indices of sites for which to calculate field (as defined from analysis)')
        
        self.mode = StringVar(self)
        mode_options=['auto','manual']
        Label(self, text="Mode").grid(row=5, column=0, sticky=W)
        self.m = ttk.OptionMenu(self, self.mode, mode_options[0], *mode_options)
        self.m.grid(row=6, column=0, sticky=W)

        self.find_equiv = IntVar()
        self.fe=Checkbutton(self, text="Find equivalent muon positions", 
        variable=self.find_equiv)
        self.fe.grid(row=9, column=0, sticky=W)
        ttk.Button(self, text='Run', command=self.calculate_fields).grid(row=13, 
        column=0, sticky=W, pady=4)

        self.outputtext = tkst.ScrolledText(self, height=10, width=100)
        self.outputtext.grid(column=0, row=14, columnspan=2)

        self.n=TextField(self, "Number of cells to use", 0, 1, '100',None)

        self.incomm = IntVar()
        self.ic=Checkbutton(self, text="Incommensurate magnetic structure?", 
        variable=self.incomm)
        self.ic.grid(row=2, column=1, sticky=W)
        
        k_label=Label(self, text="Propagation vector")
        k_label.grid(row=3, column=1, sticky=W)
        self.k=SimpleTableInput(self, 1, 3)
        self.k.grid(row=4, column=1, sticky=W)
 
        ttk.Button(self, text='Generate mag_array file', 
        command=self.gen_magarray).grid(row=5, column=1, sticky=W, pady=4)

        self.mag_array_name=TextField(self, "Name of mag_array file", 6, 1, '',None)

        ttk.Button(self, text='Check magnetic structure', 
        command=self.view_mag_struct).grid(row=8, column=1, sticky=W, pady=4)

        self.property = StringVar(self)
        mode_options=['Magnitude','Vector','Dipolar tensor']
        Label(self, text="Property to calculate").grid(row=9, column=1, sticky=W)
        self.p = ttk.OptionMenu(self, self.property, mode_options[0], *mode_options)
        self.p.grid(row=10, column=1, sticky=W)
        
        self.lorentz = IntVar()
        self.lor=Checkbutton(self, text="Include Lorentz field", 
        variable=self.lorentz)
        self.lor.select()
        self.lor.grid(row=11, column=1, sticky=W)
        
        self.shape_factor=TextField(self, "shape factor", 12,1, 0,'shape factor for demagnetizing field')
        self.grid_rowconfigure(15, weight=1) # spacing to improve appearance
        
        self.field_dist=TextField(self, "Data file for field distribution", 16, 0, '',None)
        
        ttk.Button(self, text='View plot', 
        command=self.view_plot).grid(row=18, column=0, sticky=W, pady=4)
        
        self.grid_rowconfigure(19, weight=1) # spacing to improve appearance
        
        ttk.Button(self, text='Quit', command=self.quit).grid(row=20, column=0, 
        sticky=W, pady=4)
        
        self.mode_update()
  

