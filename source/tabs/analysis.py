import logging
import threading

from tkinter import *
from tkinter import ttk
from tkinter import filedialog

import tkinter.scrolledtext as tkst

from ase import io
from functools import partial
from clustering.clustering import *
from clustering.displacements import *

from widgets.tooltip import ToolTip, ToolTipBase
from widgets.textfield import TextField

import ase.io.cif
import ase.gui.ag

import pickle

from config import *
from custom_view import *


# Taken from https://gist.github.com/moshekaplan/c425f861de7bbf28ef06
class TextHandler(logging.Handler):
    """This class allows you to log to a Tkinter Text or ScrolledText widget"""

    def __init__(self, text):
        # run the regular Handler __init__
        super().__init__()
        # Store a reference to the Text it will log to
        self.text = text

    def emit(self, record):
        msg = self.format(record)

        def append():
            self.text.insert(tk.END, msg + '\n')
            # Autoscroll to the bottom
            self.text.yview(tk.END)
        # This is necessary because we can't modify the Text from other threads
        self.text.after(0, append)


class analysis(ttk.Notebook):

    def algorithm_update(self):
        if self.algorithm.get() != self.prev_algorithm:
            if self.algorithm.get() == 'connected components':
                self.param.change_label("Tolerance for clustering",
                                        'Maximum distance between muon positions for them to be'
                                        'considered "connected"')
            if self.algorithm.get() == 'k-means':
                self.param.change_label("Number of clusters",
                                        'Expected number of clusters for k-means clustering')
            self.prev_algorithm = self.algorithm.get()
        self.after(1000, func=self.algorithm_update)

    def __init__(self, *args, **kwargs):

        def set_struct():
            try:
                input_struct = filedialog.askopenfilename()
                input.delete(0, 'end')
                input.insert(END, os.path.normpath(input_struct))
                struct = io.read(input_struct)
                logger.info("%s loaded as input cif file.", input_struct)

                try:
                    custom_view(struct)
                except UnicodeDecodeError:
                    pass
            except:
                logger.info("Error loading file.  Please try again.")

        def view_struct2():
            if aout.shifted == [None]:
                logger.info("Clustering not yet run.")
            else:
                custom_view(aout.shifted)

        def view_struct3():
            if aout.valid_struct == [None]:
                logger.info("Clustering not yet run.")
            else:
                energy.delete(0, 'end')
                muon_position_a.delete(0, 'end')
                muon_position_b.delete(0, 'end')
                muon_position_c.delete(0, 'end')
                single_struct = aout.valid_struct
                rel_energy = single_struct[int(singles.get())].get_potential_energy(
                )-single_struct[0].get_potential_energy()
                energy.insert(END, rel_energy)
                muon_position_a.insert(
                    END, aout.muon_sites[int(singles.get())][0])
                muon_position_b.insert(
                    END, aout.muon_sites[int(singles.get())][1])
                muon_position_c.insert(
                    END, aout.muon_sites[int(singles.get())][2])
                custom_view(single_struct[int(singles.get())])

        def view_disp():
            if aout.valid_struct == [None]:
                logger.info("Clustering not yet run.")
            else:
                single_struct = aout.valid_struct
                radial_disp(aout.input_struct,
                            single_struct[int(singles.get())], aout.new_label)

        def run_clustering():
            aout.input_struct = io.read(input.get())
            threading.Thread(target=cluster, args=(self.algorithm.get(),
                                                   self.shift.get(), musym.get(), self.param.get(), results.get(), output_text,
                                                   aout, self.use_disp.get(), self.max_disp.get())).start()

        def write_to_cif():
            output_cif = filedialog.asksaveasfilename(defaultextension=".cif")
            try:
                os.remove(filePath)  # remove file if it already exists
            except:
                pass
            tmp_cif = os.path.join(os.path.dirname(input.get()), 'tmp.cif')
            write(tmp_cif, aout.shifted)
            with open(output_cif, 'w') as c2:
                with open(tmp_cif, 'r') as c1:
                    for line in c1:
                        c2.write(line.replace(aout.new_label, 'H:mu'))
            os.remove(tmp_cif)
            logger.info("Saved muon sites as %s.", output_cif)

        def write_single_cif():
            init_name = os.path.basename(input.get()).replace(
                '.cif', '')+'_mu'+singles.get()+'.cif'
            single_cif = filedialog.asksaveasfilename(initialfile=init_name)
            try:
                os.remove(filePath)  # remove file if it already exists
            except:
                pass
            tmp_cif = os.path.join(os.path.dirname(input.get()), 'tmp.cif')
            single_struct = aout.valid_struct
            write(tmp_cif, single_struct[int(singles.get())])
            with open(single_cif, 'w') as c2:
                with open(tmp_cif, 'r') as c1:
                    for line in c1:
                        c2.write(line.replace(aout.new_label, 'H:mu'))
            os.remove(tmp_cif)
            logger.info("Saved muon site %s as %s.", singles.get(), single_cif)

        def load_dir():
            results_folder = filedialog.askdirectory()
            results.delete(0, 'end')
            results.insert(END, os.path.normpath(results_folder))

        def save_structures():
            pickle_file = input.get().replace('.cif', '.pkl')
            with open(pickle_file, 'wb') as f:  # Python 3: open(..., 'wb')
                pickle.dump(aout, f)
            logger.info("Saved structures to %s.", pickle_file)

        def load_structures():
            pickle_file = input.get().replace('.cif', '.pkl')
            with open(pickle_file, 'rb') as f:  # Python 3: open(..., 'rb')
                bout = pickle.load(f)
            aout.input_struct = bout.input_struct
            aout.new_label = bout.new_label
            aout.valid_struct = bout.valid_struct
            aout.muon_sites = bout.muon_sites
            aout.muon_sites_supercell = bout.muon_sites_supercell
            aout.shifted = bout.shifted
            aout.clust_composition = bout.clust_composition
            logger.info("Loaded structures from %s.", pickle_file)

        def list_sites():
            single_struct = aout.valid_struct
            min_energy = single_struct[0].get_potential_energy()
            for i in range(len(single_struct)):
                struct_energy = single_struct[i].get_potential_energy()
                rel_energy = struct_energy - min_energy
                mupos = aout.muon_sites[i]
                logger.info(
                    "Muon site %i at %s has relative energy %s eV.",
                    i, mupos, rel_energy
                )

        ttk.Frame.__init__(self, *args, **kwargs)
        Label(self, text="Input structure:").grid(row=0, column=0, sticky=W)
        input = Entry(self)
        input.grid(row=1, sticky=W)
        b = ttk.Button(self, text="load", width=10, command=set_struct).grid(
            row=2, column=0, sticky=W)

        results_lbl = Label(self, text="Results directory:")
        results_lbl.grid(row=3, column=0, sticky=W)
        results = Entry(self)
        results.grid(row=4, sticky=W)
        results_ttp = ToolTip(
            results_lbl, "folder containing output cell and .castep files")

        self.shift = IntVar()
        shift_tick = Checkbutton(self, text="Shift sites to symmetry equivalent positions",
                                 variable=self.shift)
        shift_tick.grid(row=7, column=0, sticky=W)

        b3 = ttk.Button(self, text="load", width=10, command=load_dir).grid(
            row=5, column=0, sticky=W)

        musym = TextField(self, "Symbol for muon", 0, 1, '',
                          'symbol used to represent muon in calculations')

        self.algorithm = StringVar(self)
        algorithm_options = ['connected components', 'k-means']
        Label(self, text="Clustering algorithm").grid(
            row=2, column=1, sticky=W)
        w = ttk.OptionMenu(self, self.algorithm,
                           algorithm_options[0], *algorithm_options)
        w.grid(row=3, column=1, sticky=W)

        self.param = TextField(self, "Tolerance for clustering", 4, 1, '1.0',
                               'Maximum distance between muon positions for them to be considered "connected"')

        def activateCheck():
            if self.use_disp.get() == 1:  # whenever checked
                self.max_disp.config(state=NORMAL)
                self.max_disp.insert(END, '2.0')
            elif self.use_disp.get() == 0:  # whenever unchecked
                self.max_disp.delete(0, END)
                self.max_disp.config(state=DISABLED)

        self.use_disp = IntVar()
        use_disp_tick = Checkbutton(self, text="Maximum allowed displacement",
                                    variable=self.use_disp, command=activateCheck)
        use_disp_tick.grid(row=6, column=1, sticky=W)
        self.max_disp = Entry(self)
        self.max_disp.grid(row=7, column=1, sticky=W)
        self.max_disp.config(state=DISABLED)
        disp_ttp = ToolTip(
            use_disp_tick, 'Discard structures including ionic displacements that exceed the specified value')

        output_text = tkst.ScrolledText(self, height=10, width=100)
        output_text.grid(column=0, row=9, columnspan=2)

        logger = logging.getLogger("cluster")
        logger.addHandler(TextHandler(output_text))
        logger.setLevel(logging.INFO)

        ttk.Button(self, text='Run', command=run_clustering).grid(
            row=8, column=0, sticky=W, pady=4)
        b_view2 = ttk.Button(self, text='View muon sites',
                             command=view_struct2)
        b_view2.grid(row=14, column=0, sticky=W, pady=4)

        list_button = ttk.Button(
            self, text='List muon sites', command=list_sites)
        list_button.grid(row=14, column=0, sticky=E, pady=4)

        write_cif = ttk.Button(
            self, text='Write cif file', command=write_to_cif)
        write_cif.grid(row=14, column=1, sticky=E, pady=4)
        write_ttp = ToolTip(
            write_cif, 'write cif file containing muons embedded within structure')

        save_sites = ttk.Button(self, text='Save sites',
                                command=save_structures)
        save_sites.grid(row=15, column=0, sticky=W, pady=4)
        save_ttp = ToolTip(save_sites, 'Write a .pkl file containing the results'
                           ' of the clustering analysis')

        load_sites = ttk.Button(self, text='Load sites',
                                command=load_structures)
        load_sites.grid(row=15, column=1, sticky=E, pady=4)
        load_ttp = ToolTip(
            load_sites, 'Load results of previous analysis from a .pkl file')

        self.grid_rowconfigure(16, weight=1)
        single_site_frame = LabelFrame(
            self, text="Single site analysis", pady=2)
        single_site_frame.grid(row=16, columnspan=2, sticky='WE',
                               padx=2, pady=2, ipadx=2, ipady=2)

        # use empty column to space out widgets
        single_site_frame.grid_columnconfigure(2, weight=1)

        singles_label = Label(single_site_frame, text="Site index")
        singles_label.grid(row=17, column=0, sticky=W)
        singles = Entry(single_site_frame)
        singles.grid(row=18, column=0, sticky=W)
        singles_ttp = ToolTip(
            singles_label, 'Index of muon stopping site to analyse')
        ttk.Button(single_site_frame, text='Update', command=view_struct3).grid(
            row=19, column=0, sticky=W, pady=4)
        ttk.Button(single_site_frame, text='View displacements',
                   command=view_disp).grid(row=20, column=0, sticky=W, pady=4)

        energy_label = Label(single_site_frame, text="Energy (eV)")
        energy_label.grid(row=17, column=3, sticky=E)
        energy = Entry(single_site_frame)
        energy.grid(row=18, column=3, sticky=E)
        energy_ttp = ToolTip(
            energy_label, 'Energy of muon stopping site relative to energy of lowest energy site.')

        position_label = Label(single_site_frame, text="Muon position")
        position_label.grid(row=17, column=2, sticky=W)
        muon_position_a = Entry(single_site_frame)
        muon_position_a.grid(row=18, column=2, sticky=W)
        muon_position_b = Entry(single_site_frame)
        muon_position_b.grid(row=19, column=2, sticky=W)
        muon_position_c = Entry(single_site_frame)
        muon_position_c.grid(row=20, column=2, sticky=W)
        position_ttp = ToolTip(
            position_label, 'Fractional coordinates of muon site in conventional cell')

        write_cif = ttk.Button(
            single_site_frame, text='Write structure to cif', command=write_single_cif)
        write_cif.grid(row=19, column=3, sticky=E, pady=4)
        write_ttp = ToolTip(
            write_cif, 'Write cif file containing muons embedded within structure')

        ttk.Button(self, text='Quit', command=self.quit).grid(
            row=20, column=0, sticky=W, pady=4)

        self.prev_algorithm = 'connected components'

        self.algorithm_update()
