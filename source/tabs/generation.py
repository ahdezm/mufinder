from tkinter import *
from tkinter import ttk
from tkinter import filedialog

import tkinter.scrolledtext as tkst

from widgets.tooltip import ToolTip, ToolTipBase
from widgets.table import SimpleTableInput
from widgets.get_numbers import get_numbers

from ase import io
import os
import threading

from custom_view import *
from sitegen.gen_sites import *

from widgets.tooltip import ToolTip, ToolTipBase
from widgets.table import SimpleTableInput
from widgets.textfield import TextField

from config import *

class generation(ttk.Notebook):  
     
    def __init__(self, *args, **kwargs):
        
        def set_struct():
            try:
                input_struct=filedialog.askopenfilename()
                input.delete(0, 'end')
                input.insert(END, os.path.normpath(input_struct))
                struct=io.read(input_struct)
                gen.input_struct=struct
                atom_list=list(set(struct.get_chemical_symbols()))
                menu = atom_choice["menu"]
                menu.delete(0, "end")
                for string in atom_list:
                    menu.add_command(label=string, 
                             command=lambda value=string: self.om_variable.set(value))
                outputtext.insert("end-1c","%s loaded as input cif file.\n" %input_struct)
                input_root=os.path.basename(input.get()).replace('.cif', '')
                bad_chars="() "
                for c in bad_chars:
                    input_root = input_root.replace(c,"")
                rootname.replace(input_root)
                try:
                    custom_view(gen.input_struct) 
                except UnicodeDecodeError:
                    pass     
                
            except: 
                outputtext.insert("end-1c","Error loading file.  Please try again.\n")
        
        def run_custom():
            gen.mode=self.mode.get()
            gen_custom(gen,outputtext)
     
        def run_closeto():
            if self.om_variable.get()=='':
                outputtext.insert("end-1c","Please select an element.\n")
            else:
                gen.vdws=float(vdws.get())
                thread=threading.Thread(target=gen_closeto, args=(gen,self.om_variable.get(),outputtext))
                thread.start()
            
        def symm_shift():
            thread=threading.Thread(target=gen_cluster, args=(gen,outputtext))
            thread.start()
        def run_gen():  
            gen.minr=float(minr.get())
            gen.vdws=float(vdws.get())    
            thread=threading.Thread(target=gen_random, args=(gen,outputtext))
            thread.start()
       
        def view_struct():
            if len(gen.site_list)>0:
                         
                scaled=[]
                for pos in gen.site_list:
                    scaled.append(gen.input_struct.cell @ pos)
    
                # view all muon positions within the supercell
                sm = np.array([[float(j) for j in i] for i in scell_matrix.get()]).reshape((3, 3))
                scell0 = make_supercell(gen.input_struct, sm)

                all_muons=scell0.copy()
                for mupos in scaled:
                    # Add the muon to the structure
                    all_muons +=Atoms('H', positions=[mupos])
                custom_view(all_muons)
            
            else:
                outputtext.insert("end-1c","No muon positions generated.\n")
        
        def list_sites():
            gen.mode=self.mode.get()
            if gen.mode=='absolute':
                cell_mat=gen.input_struct.get_cell()
                list2=[]
                for i in range(len(gen.site_list)):
                    list2.append(cell_mat @ gen.site_list[i])
            else:
                list2=gen.site_list
            for i in range(len(list2)):
                outputtext.insert("end-1c","Initial position %i has %s coordinates %s.\n" %(i+1,gen.mode,list2[i]))
        
        def delete_sites():
            ids=get_numbers(num.get(),outputtext)
            num_removed=0
            for id0 in ids:
                gen.site_list.pop(id0-num_removed-1)
                outputtext.insert('end-1c','Removed initial muon position %s.\n' %id0)
                num_removed+=1
                        
        def write_files():    
            gen.symbol=musym.get()
            gen.scell_mat=scell_matrix.get()
            gen.mode=self.mode.get()
            inputf_full=os.path.join(os.path.dirname(input.get()),inputf.get())
            create_input(gen,inputf_full,rootname.get(), outputtext)
             
        # generate input cell containing structure only (without a muon)
        def cell_only():
            inputf_full=os.path.join(os.path.dirname(input.get()),inputf.get())
            gen.scell_mat=scell_matrix.get()
            create_scell(gen,rootname.get(),inputf_full,outputtext)
            
        ttk.Frame.__init__(self, *args, **kwargs)

        Label(self, text="Input structure:").grid(row=0, column=0, sticky=W)
        input = Entry(self)
        input.grid(row=1, sticky=W)
   
        b = ttk.Button(self, text="load", width=10, command=set_struct).grid(row=2, column=0, sticky=W)

        inputf=TextField(self, "folder name", 3, 0, 'input',
        'folder to contain input files for calculations')
        
        rootname=TextField(self, "rootname", 5, 0, '',
        'Root name for input files')

        Label(self, text="Coordinates system:").grid(row=7, column=0, sticky=W)
        
        mode_list=['fractional','absolute']
        self.mode = StringVar(self)
        self.mode.set(mode_list[0])
        mode_choice=ttk.OptionMenu(self, self.mode, mode_list[0], *mode_list)
        mode_choice.grid(row=8, column=0, sticky=W)
     
        atom_list=['']
        self.om_variable = StringVar(self)
        self.om_variable.set(atom_list[0])
    
        atom_choice=ttk.OptionMenu(self, self.om_variable, atom_list[0], *atom_list)
        atom_choice.grid(row=10, column=0)

        musym=TextField(self, "Symbol for muon", 0, 1, 'H:mu',
        'symbol used to represent muon in calculations')
        
        minr=TextField(self, "minr", 2, 1, '0.5',
        "Minimum distance between initial muon positions")
    
        vdws=TextField(self, "VDW scale", 4, 1, '1.0',
        "Minimum distance between initial muon position and atoms in the structure")
        
        tol=TextField(self, "Tolerance for clustering", 6, 1, '1.0',
        'Maximum distance between muon positions for them to be considered "connected"')
    
        variable = StringVar(self)
        variable.set("none") # default value
        Label(self, text="Supercell matrix").grid(row=8, column=1, sticky=W)

        scell_matrix=SimpleTableInput(self, 3, 3)
        scell_matrix.grid(row=9, column=1, rowspan=3, sticky=W)

        outputtext = tkst.ScrolledText(self, height=10, width=100)
        outputtext.grid(column=0, row=14, columnspan=2)
        
        ttk.Button(self, text='Add single muon', command=run_custom).grid(row=9, column=0, sticky=W, pady=4)
        ttk.Button(self, text='Attach muon to element', command=run_closeto).grid(row=10, column=0, sticky=W, pady=4)
        ttk.Button(self, text='Generate random positions', command=run_gen).grid(row=11, column=0, sticky=W, pady=4)
        ttk.Button(self, text='Relocate muon sites to symmetry-reduced subspace', command=symm_shift).grid(row=12, column=0, sticky=W, pady=4)

        b_view=ttk.Button(self, text='View initial muon positions', command=view_struct)
        b_view.grid(row=15, column=0, sticky=W, pady=4)
        
        b_view=ttk.Button(self, text='Create input files', command=write_files)
        b_view.grid(row=15, column=1, sticky=W, pady=4)
       
        b_view=ttk.Button(self, text='Create .cell file', command=cell_only)
        b_view.grid(row=16, column=1, sticky=W, pady=4)
        
        ttk.Button(self, text='List initial muon positions', command=list_sites).grid(row=16, column=0, sticky=W, pady=4)
        
        num=TextField(self, "Initial position no.", 17, 0, '',
        'Identification number of an initial muon position within the list')
        
        cancel=ttk.Button(self, text='Delete muons', command=delete_sites)
        cancel.grid(row=18, column=0, sticky=E, pady=4)
        
        ttk.Button(self, text='Quit', command=self.quit).grid(row=19, column=0, sticky=W, pady=4)
    


  
        
        