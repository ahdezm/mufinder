import subprocess
import shlex

import tempfile

from platform import system
if system() != 'Windows':
    import pexpect 


def ssh(address, command, password, cipher):

    if system() == 'Windows': 
        if cipher.decrypt(password).decode()!='':
            plink='echo y | plink.exe -ssh -pw %s %s "%s"' %(cipher.decrypt(password).decode(),address,command)
        else:
            plink='echo y | plink.exe -ssh %s "%s"' %(address,command) # for use with pagent
        p=subprocess.Popen(shlex.split(plink), stdin=subprocess.PIPE, 
    stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True, shell=True)
        output,error=p.communicate()
        p.wait()
    else:
        fname = tempfile.mktemp()                                                                                                                                                  
        fout = open(fname, 'w')                                                                                                                                                    
        ssh_cmd = 'ssh %s "%s"' % (address, command)                                                                                                                 
        child = pexpect.spawnu(ssh_cmd, timeout=120) 
        if cipher.decrypt(password).decode()!='':
            child.expect(['password: ','passphrase'])                                                                                                                                                                                                                                                                                               
            child.sendline(cipher.decrypt(password).decode())                                                                                                                                                
        child.logfile = fout                                                                                                                                                    
        child.expect(pexpect.EOF)                                                                                                                                                  
        child.close()                                                                                                                                                              
        fout.close()                                                                                                                                                               

        fin = open(fname, 'r')                                                                                                                                                     
        output = fin.read()                                                                                                                                                      
        fin.close()                                                                                                                                                                

        error=output # both stderr and stdout are in logfile
        
    print(error.lstrip())
    return output.lstrip(),error.lstrip()
    
def scp(file, destination, password,cipher,recursive):
    
    if recursive==True:
        r='-r'
    else: 
        r=''
        
    if system() == 'Windows': 
        if cipher.decrypt(password).decode()!='':
            pscp='echo y | pscp.exe ' +r+' -pw %s "%s" "%s"' %(cipher.decrypt(password).decode(),file,destination)
        else:
            pscp='echo y | pscp.exe ' +r+' "%s" "%s"' %(file,destination) # for use with pagent
        subprocess.run(shlex.split(pscp),stdin=subprocess.PIPE, 
                stdout=subprocess.PIPE, stderr=subprocess.PIPE,shell=True)
    else:
        fname = tempfile.mktemp()                                                                                                                                                  
        fout = open(fname, 'w')                                                                                                                                                    
        scp_cmd = 'scp '+r+' "%s" "%s"' %(file, destination)                                                                                                                 
        child = pexpect.spawnu(scp_cmd, timeout=120) 
        if cipher.decrypt(password).decode()!='':
            child.expect(['password: '])                                                                                                                                                                                                                                                                                               
            child.sendline(cipher.decrypt(password).decode())                                                                                                                                                
        child.logfile = fout                                                                                                                                                    
        child.expect(pexpect.EOF)                                                                                                                                                  
        child.close()                                                                                                                                                              
        fout.close()                                                                                                                                                               

        fin = open(fname, 'r')                                                                                                                                                     
        output = fin.read()                                                                                                                                                      
        fin.close()           
        
def check_password(address, password, cipher):
    if system() == 'Windows': 
        plink='echo y | plink.exe -ssh -pw %s %s "exit"' %(cipher.decrypt(password).decode(),address)
        p=subprocess.Popen(shlex.split(plink), stdin=subprocess.PIPE, 
    stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True, shell=True)
        output,error=p.communicate()
        p.wait()
        if 'password:' in output:
            return False
        else:
            return True
            
    else:                                                                                                                                                   
        ssh_cmd = 'ssh %s' %address                                                                                                                 
        child = pexpect.spawnu(ssh_cmd, timeout=120)
        if cipher.decrypt(password).decode()!='':
            child.expect(['password: '])                                                                                                                                                                                                                                                                                               
            child.sendline(cipher.decrypt(password).decode())
                                                                                                                                                    
        i=child.expect(['Permission denied','Last login:'])
        
        if i==0:
            return False
        elif i==1:
            return True

            
 
def submit_job(submission_sys,username,working_dir, site_no,address,pw, cipher_suite):
    if submission_sys=='slurm':
        scriptname='script.slurm'
        sub_comm='sbatch %s' %scriptname
            
    if submission_sys=='pbs':
        scriptname='script.pbs'
        sub_comm='qsub %s' %scriptname
               
    run_command='cd %s/site%i/; %s;' %(working_dir, site_no,sub_comm)
    output,error=ssh(address, run_command,pw, cipher_suite) 
     
    if submission_sys=='pbs':
        id_sdb=output.split()[-1]
        job_id=[int(s) for s in id_sdb.split('.') if s.isdigit()][0]
    else:
        job_id=[int(s) for s in output.split() if s.isdigit()][0]
    
    return job_id
    
def copy_queue(submission_sys,username,address,pw,cipher_suite):
    # get string resulting from check of the queue
    if submission_sys=='slurm':
        comm='squeue -u %s' %username

    if submission_sys=='pbs':
        comm='qstat -u %s' %username
    
    queue_str,qerror=ssh(address,comm,pw,cipher_suite)
    
    return queue_str 
                   
def search_queue(id,queue_str,submission_sys):
    if submission_sys=='slurm': 
        id_plus=str(id)
    if submission_sys=='pbs':
        id_plus=str(id)

    if id_plus in queue_str:
        inqueue=True
        matched_line = [line for line in queue_str.split('\n') if id_plus in line]
        if submission_sys=='slurm':
            job_status=shlex.split(matched_line[0])[4]
        if submission_sys=='pbs':
            job_status=shlex.split(matched_line[0])[9] 
                              
        if job_status=='PD' or job_status=='Q':
            status='waiting'
        elif job_status=='R':
            status='running'
    else:
        inqueue=False  
        status='null'

    return inqueue,status
                                                                                                                        
                                                                                                                                                          

