import os, sys
import math

def write_script_old(hpc_name, dest, short, num_cores, jobname, proj_code):
    if hpc_name=='scarf.rl.ac.uk':
        contents="""#!/bin/bash

#SBATCH  -J  %s
#SBATCH  -n  %s
#SBATCH  -p  scarf

job_name=%s

module load devmodules

module load castep/18.1-openmpi-3.1.0

mpirun -q castep.mpi $job_name -srun""" %(short, num_cores, jobname)

        script=os.path.join(dest,'script.slurm')
        with open(script,'w',newline='\n') as f:
            f.write(contents)
     
    if hpc_name=='hamilton.dur.ac.uk':   
        contents="""#!/bin/bash

#SBATCH  -J  %s
#SBATCH  -n  %s
#SBATCH  -p  par7.q

job_name=%s

module purge
module load slurm
module load castep

mpirun -np %s castep.mpi $job_name""" %(short, num_cores, jobname, num_cores)
    
        script=os.path.join(dest,'script.slurm')
        with open(script,'w',newline='\n') as f:
            f.write(contents)
            
    if hpc_name=='login.archer.ac.uk':
        
        num_nodes=math.ceil(float(num_cores)/24)
        contents="""#!/bin/bash --login

# PBS job options (name, compute nodes, job time)
#PBS -l select=%s
#PBS -l walltime=48:00:00
#PBS -q long

# Replace [project code] below with your project code (e.g. t01)
#PBS -A %s

# Make sure any symbolic links are resolved to absolute path
export PBS_O_WORKDIR=$(readlink -f $PBS_O_WORKDIR)               
  
# Change to the directory that the job was submitted from
# (remember this should be on the /work filesystem)
cd $PBS_O_WORKDIR

# Set scratch dir
export TMPDIR=$PWD

# Load the CASTEP module
module load castep 

# Set the number of threads to 1
#   This prevents any system libraries from automatically 
#   using threading.
export OMP_NUM_THREADS=1

# Launch the parallel job
aprun -n %s castep.mpi %s""" %(num_nodes,proj_code, num_cores,jobname)

        script=os.path.join(dest,'script.pbs')
        with open(script,'w',newline='\n') as f:
            f.write(contents)

def write_script(hpc_name, submission_system, dest, short, num_cores, jobname, cores_per_node, time):           

    #get number of nodes from number of cores
    nodes=math.ceil(float(num_cores)/24)
    
    #get path for template files
    dir_name=os.path.join(os.path.abspath(os.path.dirname(sys.argv[0])),'script_templates')
    template=os.path.join(dir_name,'%s.%s' %(hpc_name,submission_system))
    
    with open(template, 'r') as myfile:
        script_main = myfile.read()
    
        var = {"short": short, "num_cores": num_cores, "jobname": jobname, "nodes": nodes, "time" : time}
   
        contents=script_main.format(**var)
        
        script=os.path.join(dest,'script.%s' %submission_system)
        with open(script,'w',newline='\n') as f:
            f.write(contents)
            
    
