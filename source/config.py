# List of variables that are accessible across all modules i.e. across all tabs of the 
# GUI

from containers import mu_gen, mu_struct
import os, sys

gen=mu_gen()
aout=mu_struct()

final_struct=[None]
muons_for_fields=[]
muons_for_fields2=[]
index_for_fields=[]
dipole_fields=[None]
current_code="CASTEP"
current_hpc="local"

# read information from config file which is editable by the user
from configobj import ConfigObj
dir_name=os.path.abspath(os.path.dirname(sys.argv[0]))
cfg = ConfigObj(os.path.join(dir_name,'mufinder.ini'))

# get full path of mpirun executable
mpi_exec=cfg['MPI']['mpi_loc']

# get paths of CASTEP executables
castep_serial=cfg['CASTEP']['castep_serial_loc']
castep_mpi=cfg['CASTEP']['castep_mpi_loc']

hpc_options=cfg['HPC']

hpc_names=[cluster for cluster in cfg['HPC']]


