from ase import io
    
def write_kpoints(mpgrid):
    try:
        mpgrid=[int(k) for k in mpgrid[0]]
    except:
        mpgrid=[0,0,0]
    if all(k>0 for k in mpgrid):
        return "\nkpoints_mp_grid %i %i %i" %(mpgrid[0],mpgrid[1],mpgrid[2]) 
    else:
        return ""
     
def write_to_cell(cell_text,cell_file,mpgrid):
    cell_text=write_kpoints(mpgrid)+'\n'+cell_text
    with open(cell_file, "a") as f:
        f.write(cell_text)
                      
def param_text(charge,xc,spin_pol,basis_prec,solver):
    param_text='task: geometryoptimisation\n'
    if charge=='mu+':
        param_text+='charge: +1\n'
    param_text+='xc_functional: %s\n' %xc
    if spin_pol==True:
        param_text+='spin_polarized : true\n'
    else:
        param_text+='spin_polarized : false\n'
    param_text+='basis_precision : %s\n' %basis_prec
    param_text+='write_cell_structure : true\n'
    param_text+='geom_max_iter : 300\n'
    param_text+='max_scf_cycles : 100\n'
    if solver=='edft':
        param_text+='elec_method : edft\n'
    param_text+='continuation : default'
    return param_text
    
def cell2cif(cell_file,outputtext):
    structure=io.read(cell_file)
    cifname=cell_file.replace('_0-out.cell','_relaxed.cif')
    io.write(cifname,structure)
    outputtext.insert("end-1c",'Relaxed structure can be found in %s.\n' %cifname)
    