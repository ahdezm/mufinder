"""A script to generate initial positions muon positions in a symmetry-reduced
subspace of the unit cell"""

import os

import numpy as np
import spglib as spg

from numpy import linalg as LA
from tkinter import simpledialog

from ase import *
from ase import io, Atoms
from ase.calculators.castep import Castep
from ase.build.supercells import make_supercell
from ase.geometry import cell_to_cellpar, get_distances
from ase.spacegroup import Spacegroup, crystal

from custom_view import *
from config import *

# function to generate symmetry equivalent positions
def gen_sym_equiv(basis, structure, label):
    spacegroup=spg.get_symmetry_dataset(structure)
    replicate = crystal(label, basis,spacegroup=spacegroup['number'], 
    cellpar=cell_to_cellpar(structure.get_cell())) 
    return replicate
 
# function to generate single muon in cell
def gen_single_muon(basis, structure, label):
    replicate = crystal(label, basis, spacegroup=1,
    cellpar=cell_to_cellpar(structure.get_cell())) 
    return replicate

def gen_custom(gen,outputtext):
    muon_sites=gen.site_list.copy()
    a = simpledialog.askstring("Input",'Enter %s coordinates for initial muon position.\n' %gen.mode)
    try:
        new_pos=[float(x) for x in a.split()]
        outputtext.insert("end-1c",'Added muon at position with %s coordinates %s.\n' %(gen.mode,new_pos))
        if gen.mode=='absolute':
            cell_lengths=gen.input_struct.get_cell_lengths_and_angles()
            new_pos_tmp=[new_pos[i]/cell_lengths[i] for i in range(len(new_pos))]
            new_pos=new_pos_tmp
        muon_sites.append(new_pos)
        gen.site_list=muon_sites
         
    except ValueError:
        outputtext.insert("end-1c",'Coordinates entered incorrectly. Please try again.\n')

   
def gen_random(gen, outputtext):
    input_struct = gen.input_struct
    minr = gen.minr
    vdws = gen.vdws
    muon_sites = gen.site_list.copy()

    outputtext.insert(
        "end-1c",
        ("Generating random configurations with the muons being"
         "at least {} angstroms from each other and at least {}"
         " angstroms away from other atoms in the cell...\n")
        .format(minr, vdws)
    )

    attempts = 0  # counts number of times we try to add a new muon but fail

    if len(muon_sites) == 0:
        muon_sites.append(np.random.random((3,)))

    muon_pos = input_struct.cell.cartesian_positions(muon_sites)

    sg_N = spg.get_symmetry_dataset(input_struct)["number"]
    sg = Spacegroup(sg_N, 1)

    while attempts < 30:
        cand_frac = np.random.random((3,))
        cand_pos = input_struct.cell.cartesian_positions(cand_frac)
        cand_equiv_frac, _ = sg.equivalent_sites(
            cand_frac,
            onduplicates="warn",
            symprec=0.001
        )
        cand_equiv_pos = input_struct.cell.cartesian_positions(cand_equiv_frac)

        _, cand_muon_dists = get_distances(
            cand_equiv_pos, muon_pos,
            cell=input_struct.cell,
            pbc=input_struct.pbc
        )

        _, cand_struct_dists = get_distances(
            cand_pos, input_struct.positions,
            cell=input_struct.cell,
            pbc=input_struct.pbc
        )

        min_muon_dist = np.min(cand_muon_dists)
        min_struct_dist = np.min(cand_struct_dists)

        if min_muon_dist >= minr and \
           min_struct_dist >= vdws:
            muon_sites.append(cand_frac)
            muon_pos = np.append(muon_pos, [cand_pos], axis=0)
            # print("Accepted: ", cand_pos, min_muon_dist, min_struct_dist)
        else:
            attempts += 1
            # print("Rejected: ", cand_pos, min_muon_dist, min_struct_dist)

    outputtext.insert(
        "end-1c",
        'Generated {0} additional configurations.\n'.format(
            len(muon_sites) - len(gen.site_list)
        )
    )

    gen.site_list = muon_sites


def gen_cluster(gen,outputtext): 
    input_struct=gen.input_struct
    muon_sites=gen.site_list.copy()
    outputtext.insert("end-1c",'Shifting muon positions to occupy smaller '
     'subspace of the unit cell...\n')
    # find muon site that is closest to the origin, but still inside cell
    dist_origin=[]
    for site in muon_sites:
        replica_i=gen_sym_equiv(site, input_struct, ['O'])
        distances=[]
        for single_mu in replica_i: 
            muon_cart=single_mu.position
            if all(coord>=0 for coord in muon_cart):
                distances.append(LA.norm(muon_cart))
        dist_origin.append(min(distances))
    origin_index=np.argmin(dist_origin)

	# translate each muon site to be closest to the site that 
	# is closest to the origin
    replica_1=gen_sym_equiv(muon_sites[origin_index], input_struct, ['H']) 
    distances=[]
    shifted=[]
    muon_cart_pos=[]
    for single_mu in replica_1:   
            muon_cart=single_mu.position 
            if all(coord>=0 for coord in muon_cart):
                muon_cart_pos.append(muon_cart)
                distances.append(LA.norm(muon_cart))
    muon_frac_pos=LA.inv(replica_1.cell) @ muon_cart_pos[np.argmin(distances)]
    replica_1=gen_sym_equiv(muon_frac_pos, input_struct, ['H'])
    for site_i in muon_sites:
        replica_i=gen_sym_equiv(site_i, input_struct, ['O'])
        combined=replica_i+replica_1
        atoms1 = [x for x in range(len(combined.get_atomic_numbers())) 
        if combined.get_chemical_symbols()[x]=='O']
        atoms2 = [y for y in range(len(combined.get_atomic_numbers())) 
        if combined.get_chemical_symbols()[y]=='H']
        distances=combined.get_distances(atoms1, atoms2[0], mic=False)
        shifted.append(replica_i.get_positions()[np.argmin(distances)])
          
    tmp_sites=[]
    for mupos in shifted:
             tmp_sites.append(LA.inv(input_struct.cell) @ mupos)

    gen.site_list=tmp_sites
    outputtext.insert("end-1c",'Done.\n')

def create_input(gen,folder,rootname, outputtext):
    input_struct=gen.input_struct
    muon_sites=gen.site_list
    musym=gen.symbol
    scell_matrix=gen.scell_mat
    if gen.mode=='fractional':
        pos_frac=True
    else:
        pos_frac=False
    
    scaled=[]
    for pos in muon_sites:
        scaled.append(input_struct.cell @ pos)
    
    # Muon mass and gyromagnetic ratio
    mass_block = 'AMU\n{0}       0.1138000000'
    gamma_block = 'radsectesla\n{0}        851586494.1'

    # Check if chosen symbol for muon conflicts with elements in unit cell
    if musym in input_struct.get_chemical_symbols():
        outputtext.insert("end-1c",'WARNING: chosen muon symbol already appears in'
              ' unit cell.\n')
 
    # Create a CASTEP calculator
    ccalc = Castep()
    ccalc.cell.species_mass = mass_block.format(musym).split('\n')
    ccalc.cell.species_gamma = gamma_block.format(musym).split('\n')
    ccalc.cell.fix_all_cell=True
        
    if len(scaled)>0:
        # generate supercell
        sm = np.array([[float(j) for j in i] for i in scell_matrix]).reshape((3, 3))
        scell0 = make_supercell(input_struct, sm)

        # Clean up any existing structures
        #try:
        #    shutil.rmtree(folder)
        #except OSError:
        #    pass
        try:
            os.makedirs(folder)
        except FileExistsError:
        # directory already exists
            outputtext.insert("end-1c","WARNING! Folder %s already exists.\n" %folder)

        # Create the supercell versions and save them
        for i, mupos in enumerate(scaled):
            # Add the muon to the structure
            scell = scell0.copy() + Atoms('H', positions=[mupos])
            # Add castep custom species
            csp = scell0.get_chemical_symbols() + [musym]
      
            scell.set_array('castep_custom_species', np.array(csp))
            scell.set_calculator(ccalc)
          
            io.write(os.path.join(folder, '{0}_{1}.cell'.format(rootname,
                                                              i+1)),
                   scell,positions_frac=pos_frac)
       
        outputtext.insert("end-1c",'Input files written to %s.\n' %folder)
     
    else:
        outputtext.insert("end-1c",'No muon positions!\n')
 
def create_scell(gen,rootname,folder,outputtext):
    
    input_struct=gen.input_struct
    scell_matrix=gen.scell_mat
    
    if gen.mode=='fractional':
        pos_frac=True
    else:
        pos_frac=False
        
    # Create a CASTEP calculator
    ccalc = Castep()
    
    sm = np.array([[float(j) for j in i] for i in scell_matrix]).reshape((3, 3))
    scell = make_supercell(input_struct, sm)

    try:
        os.makedirs(folder)
    except FileExistsError:
        # directory already exists
        outputtext.insert("end-1c","WARNING! Folder %s already exists.\n" %folder)

    scell.set_calculator(ccalc)
    fname=os.path.join(folder, '{0}_{1}.cell'.format(rootname,
                                                              0))  
    io.write(fname,scell,positions_frac=pos_frac)
       
    outputtext.insert("end-1c",'Input structure written to CELL file %s.\n' %fname)
    
def gen_main(struct, muon_sites, musym, minr, vdws, 
            tol,folder, rootname, random, sym_red,close_to, element, scell_matrix, 
            outputtext,final_struct):
    if random==True:
        gen_random(struct, minr, vdws, muon_sites, outputtext)
        
    if sym_red==True:
        gen_cluster(struct,muon_sites, tol, outputtext)
    
    if close_to==True:
        gen_closeto(struct, element, vdws, muon_sites, outputtext)

    outputtext.insert("end-1c",'Done! Click "View initial muon positions" to see sites generated.\n')

def gen_closeto(gen, element,outputtext):
    input_struct=gen.input_struct
    vdws=gen.vdws
    
    outputtext.insert("end-1c","Adding muon positions that are %s angstroms from each of "
    "the %s atoms in the unit cell.\n" %(vdws,element))
    
    
    elem_pos=[atom.position for atom in input_struct if atom.symbol==element]
    muon_sites=gen.site_list.copy()
    for atom_pos in elem_pos:
    
        valid_pos=False
        
        while valid_pos==False:
            # generate random vector with length equal to vdws
            vec = np.random.randn(1, 3)[0]
            vec /= np.linalg.norm(vec, axis=0)
            vec *= vdws

            # convert position of atom to cartesians and add vector
            new_pos=vec+atom_pos
        
            check_struct=input_struct.copy()+Atoms('Am', positions=[new_pos])
            # remove atoms that we want to be closer to
            del check_struct[[atom.index for atom in input_struct if atom.symbol==element]]
            atoms1 = [x for x in range(len(check_struct.get_atomic_numbers())) 
            if check_struct.get_chemical_symbols()[x]!='Am']
        
            atoms2=[x for x in range(len(check_struct.get_atomic_numbers()))
		    if check_struct.get_chemical_symbols()[x]=='Am']
        
            distances_check=(np.min(check_struct.get_distances(atoms1, atoms2[0],mic=True)))
            if distances_check>=1.6*vdws: 
            #factor 1.6 reflects typical coordination geometries
                muon_sites.append(LA.inv(input_struct.cell) @ new_pos)
                valid_pos=True
        
    outputtext.insert("end-1c",'Generated {0} additional configurations.\n'.format(len(muon_sites)-len(gen.site_list)))
    gen.site_list=muon_sites
    
