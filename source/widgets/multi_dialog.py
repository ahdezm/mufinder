from tkinter import *
from tkinter import ttk

import tkinter.scrolledtext as tkst
   
class CustomDialog(Toplevel):
    def __init__(self, parent, default, txt):
        Toplevel.__init__(self, parent)

        self.label = Label(self, text=txt)
        self.entry= tkst.ScrolledText (self, height=5)
        self.ok_button = ttk.Button(self, text="Save", command=self.on_ok)

        self.label.pack(side="top", fill="x")
        self.entry.pack(side="top", fill="x")
        self.ok_button.pack(side="right")

        self.entry.insert("end-1c",default)
        self.entry.see("end")
        self.var=self.entry.get(0.0,END)

    def on_ok(self, event=None):
        self.var=self.entry.get(0.0,END)
        self.destroy()

    def show(self):
        self.wm_deiconify()
        self.entry.focus_force()
        self.wait_window()
        return self.var
    
