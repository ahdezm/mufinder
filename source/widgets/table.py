from tkinter import *

def convert_to_float(frac_str):
    try:
        return float(frac_str)
    except ValueError:
        try:
            num, denom = frac_str.split('/')
        except ValueError:
            return None
        try:
            leading, num = num.split(' ')
        except ValueError:
            return float(num) / float(denom)        
        if float(leading) < 0:
            sign_mult = -1
        else:
            sign_mult = 1
        return float(leading) + sign_mult * (float(num) / float(denom))

class SimpleTableInput(Frame):
    def __init__(self, parent, rows, columns):
        Frame.__init__(self, parent)

        self._entry = {}
        self.rows = rows
        self.columns = columns

        # register a command to use for validation
        vcmd = (self.register(self._validate), "%P")

        # create the table of widgets
        for row in range(self.rows):
            for column in range(self.columns):
                index = (row, column)
                e=Entry(self)
                #e = Entry(self, validate="key", validatecommand=vcmd)
                e.grid(row=row, column=column, stick="nsew")
                e['width']=8
                if self.rows==3:
                    if row==column:
                        e.insert(END, '1.0')
                    else:
                        e.insert(END, '0.0')
                else:
                    e.insert(END, '0')
                self._entry[index] = e
        # adjust column weights so they all expand equally
        for column in range(self.columns):
            self.grid_columnconfigure(column, weight=1)
        # designate a final, empty row to fill up any extra space
        self.grid_rowconfigure(rows, weight=1)

    def get(self):
        '''Return a list of lists, containing the data in the table'''
        result = []
        for row in range(self.rows):
            current_row = []
            for column in range(self.columns):
                index = (row, column)
                current_row.append(convert_to_float(self._entry[index].get()))
            result.append(current_row)
        return result

    def _validate(self, P):
        '''Perform input validation. 

        Allow only an empty value, or a value that can be converted to a float
        '''
        if P.strip() == "":
            return True
        try:
            f = convert_to_float(P)
        except ValueError:
            self.bell()
            return False
        return True
        
    def disable(self):
        for row in range(self.rows):
           for column in range(self.columns):
               index=(row, column)
               self._entry[index].config(state=DISABLED)
     
    def enable(self):
        for row in range(self.rows):
           for column in range(self.columns):
               index=(row, column)
               self._entry[index].config(state=NORMAL)  
    
    def set(self,entry):  
        for row in range(self.rows):
           for column in range(self.columns):
               index=(row, column)
               self._entry[index].delete(0, END)
               self._entry[index].insert(0, entry)      
               
 
 