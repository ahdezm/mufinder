from tkinter import *
from tkinter import ttk

from tabs.generation import *
from tabs.analysis import *
from tabs.dipolefield import *
from tabs.run_CASTEP import *

from config import *

root = Tk()
root.title('MuFinder')
root.resizable(False,False)

# set icon for Windows
from platform import system
if system() == 'Windows':
    root.iconbitmap('mufinder_logo.ico')

# set icon for Linux
if system() == 'Linux':
    icon=PhotoImage(file='mufinder_logo.png')
    root.tk.call('wm','iconphoto', root._w, icon)
   
nb = ttk.Notebook(root)
page1 = generation(nb)
page2 = run_CASTEP(nb)
page3 = analysis(nb)
page4 = dipolefield(nb)

nb.add(page1, text='Generation')
nb.add(page2, text='Run')
nb.add(page3, text='Analysis')
nb.add(page4, text='Dipole Field')
nb.grid(row=1, column=1, sticky='E', padx=5, pady=5, ipadx=5, ipady=5)   

def hpc_update():
    global current_hpc
    if page2.mode.get()=='local':
        page2.hpc_address=''
        if page2.mode.get()!=current_hpc:
            page2.working_folder.replace('')
            page2.username.replace('')
    else:
        if page2.mode.get()!=current_hpc:
            page2.hpc_address=hpc_options[page2.mode.get()]['address']
            page2.submission_sys=hpc_options[page2.mode.get()]['queue_system'].casefold()
            page2.working_folder.replace(hpc_options[page2.mode.get()]['working_dir'])
            page2.username.replace(hpc_options[page2.mode.get()]['username'])
            try:
                page2.login_name=hpc_options[page2.mode.get()]['login_name']
                page2.use_login_name=True
            except:
                pass
            page2.cores_per_nodes=hpc_options[page2.mode.get()]['cores_per_node']
            page2.walltime.replace(hpc_options[page2.mode.get()]['walltime'])
    current_hpc=page2.mode.get()
    root.after(1000, func=hpc_update)

root.after(0, func=hpc_update)

# There is currently a bug with matplotlib objects (such as ASE GUI) in tkinter that
# results in the GUI crashing when scrolling while over the object.  This workaround
# catches the exception and prevents the crash.
while True:
    try:
        root.mainloop()
        break
    except UnicodeDecodeError:
        pass





