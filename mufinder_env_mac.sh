#!/bin/bash
# for mac

python3 -m venv mufinder-env
source mufinder-env/bin/activate

pip install ase==3.19.2
pip install pandas
pip install spglib
pip install networkx
pip install soprano
pip install configobj
pip install cryptography
pip install pyinstaller
pip install tornado
pip install pexpect

# newer versions of matplotlib don't play nicely with pyinstaller
pip uninstall --yes matplotlib
pip install matplotlib==3.1

cd mufinder-env/lib/python3.7/site-packages

curl https://codeload.github.com/bonfus/muLFC/tar.gz/master | \
  tar -xz
mv muLFC-master muLFC
cd muLFC
python3 setup.py build
python3 setup.py test
python3 setup.py install
cd ..

curl https://codeload.github.com/bonfus/muesr/tar.gz/master | \
  tar -xz
mv muesr-master muesr
cd muesr
python3 setup.py build
python3 setup.py test
python3 setup.py install
cd ..

# make plutonium black
sed -i.bak 's/0.000,0.420,1.000/0.000,0.000,0.000/g' ase/data/colors.py

# make plutonium smaller
sed -i.bak 's/1.87,  # Pu/0.31,  # Pu/g' ase/data/__init__.py 

# enable velocities (magnetic moments) in GUI by default
sed -i.bak '464s/False/True/' ase/gui/gui.py

cd ../../../..
