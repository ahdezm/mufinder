# set up development virtual environment for Windows
python -m venv mufinder-env

cd mufinder-env
Scripts/Activate.ps1

python -m pip install --upgrade pip

pip install ase==3.19.2
pip install pandas
pip install spglib
pip install networkx
pip install soprano
pip install configobj
pip install cryptography
pip install pyinstaller
pip install tornado
pip install pypiwin32

# newer versions of matplotlib don't play nicely with pyinstaller
pip uninstall --yes matplotlib
pip install matplotlib==3.1

cd Lib/site-packages

curl https://codeload.github.com/bonfus/muLFC/tar.gz/master -o muLFC.txt
tar -f muLFC.txt -xz
rm muLFC.txt
mv muLFC-master muLFC
cd muLFC
python setup.py build
python setup.py test
python setup.py install
cd ..

curl https://codeload.github.com/bonfus/muesr/tar.gz/master -o muesr.txt
tar -f muesr.txt -xz
rm muesr.txt
mv muesr-master muesr
cd muesr
python setup.py build
python setup.py test
python setup.py install

cd ../../..

# fix Windows issue with ASE's castep calculator
((Get-Content -path ./Lib/site-packages/ase/calculators/castep.py) -replace "if not line or out.tell().+","if 'Peak Memory Use' in line:") | 
Set-Content -Path ./Lib/site-packages/ase/calculators/castep.py

# make plutonium black
((Get-Content -path ./Lib/site-packages/ase/data/colors.py) -replace "0.000,0.420,1.000", "0.000,0.000,0.000") | Set-Content -Path ./Lib/site-packages/ase/data/colors.py

# make plutonium smaller
((Get-Content -path ./Lib/site-packages/ase/data/__init__.py) -replace "1.87,  # Pu", "0.31,  # Pu") | Set-Content -Path ./Lib/site-packages/ase/data/__init__.py

# enable velocities (magnetic moments) in GUI by default
 $filecontent = Get-Content -Path ./Lib/site-packages/ase/gui/gui.py
 $filecontent[463] = $filecontent[463] -replace 'False','True'
 Set-Content ./Lib/site-packages/ase/gui/gui.py -Value $filecontent

cd ..